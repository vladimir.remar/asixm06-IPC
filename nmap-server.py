#!/usr/bin/python 
#-*- coding: utf-8-*-
# server nmap
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# @edt Curs 2017-2018
# Vladimir remar
# -----------------------------------------------------------------
import sys,socket
import os,signal,time

HOST = ''
PORT = 50001
_DATE_FORMAT = "%Y-%m-%d-%H:%M:%S"

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)

print os.getpid()

while True:
  try:
    conn, addr = s.accept()
    t = time.strftime(_DATE_FORMAT, time.localtime(time.time()))
    name_fit ='HOST:%s-%s'%(addr[0],t)
    ff = open(name_fit, 'w')# 'ab' to create if no exists or open to append
  except Exception:
    continue
  try:
    while True:
      data = conn.recv(1024)
      if not data: break
      ff.write(data)
    ff.close()
  except:
    continue
  conn.close()
    


sys.exit(0)

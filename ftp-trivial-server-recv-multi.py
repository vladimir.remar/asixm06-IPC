#!/usr/bin/python 
#-*- coding: utf-8-*-
# server trivial FTP MULTI mode demoni amb signals
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# @edt Curs 2017-2018
# Vladimir Remar
"""
Requeriments per al servidor:
● Se li passa com a argument (​ p n) el número de port que ha d'escoltar.
● Admet un sol client a l'hora.
● Accepta les següents ordres del client:
○ ls [path]. Retorna al client el llistat de fitxers del directori actiu.
○ get [path]nom_fitxer. Retorna al client el contingut de nom_fitxer.
● Quan el client acaba, el servidor es queda esperant una altra connexió.
● Acaba ordenadament quan rep un SIGTERM.
"""
# Features added:
#   Mode demoni
#   Gobernat per signals (sigterm,sigusr1)
#   Un RECV de dades "complert"
#   Afegit un "debug"
# -----------------------------------------------------------------

import sys,socket,time
import os,argparse,signal,select
from subprocess import Popen, PIPE

def mysigterm(signum,frame):
  print 'Signal handler called with signal', signum
  print 'Servei tancat'
  sys.exit(0)
def mysigusr1(signum,frame):
  print "Signal handler called with signal:", signum
  global clients
  if len(clients) >0:
    for client in clients:
      for i in range(0,(len(clients[client]['time'])-1)):
        print "ip: %s port: %s time: %s"%(client,clients[client]['port'][i],clients[client]['time'][i])
  else:
    sys.stderr.write('clients:0'+'\r\n')
signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGTERM, mysigterm)
#signal.signal(signal.SIGINT, signal.SIG_IGN)

parser = argparse.ArgumentParser(description='Servidor FTP trivial')
parser.add_argument('-p','--port', dest='port', help="número de port que ha d'escoltar", type=int,metavar="port-a-escoltar", required=True)
parser.add_argument('-s','--host', dest='host', help="Host per on es conecta", type=str,metavar="host",required=True)
parser.add_argument("-d", "--debug", action="store_true", help="opcio perfer el debug del programa") 

args=parser.parse_args()

HOST = args.host
PORT = args.port
EOT  = chr(4)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)
conns=[s]
clients={}

if args.debug: sys.stderr.write("Inici del programa principal PARE"+'\r\n')
pid = os.fork()
if pid != 0:
  if args.debug: sys.stderr.write("Fi Programa PARE %s %s" %(os.getpid(), pid)+'\r\n')
  sys.exit(0)
print os.getpid()
while True:
  try:
    actius,x,y = select.select(conns,[],[])
  except Exception:
    continue
  for actual in actius:
    if actual == s:
      conn, addr = s.accept()
      ip,port=addr
      t = time.strftime("%Y-%m-%d--%H:%M:%S")
      if args.debug: print clients
      if not clients.has_key(ip):
        clients[ip]={'port':[port],'time':[t]}
      else:
        clients[ip]['time'].append(t)
        clients[ip]['port'].append(port)
      if args.debug: sys.stderr.write('Connected by %s,%s'%(addr)+'\r\n')
      conns.append(conn)
    else:
      try: #afegit pels problemas amb els signals
        data = actual.recv(1024)
        if not data: 
          if args.debug: sys.stdout.write("Client finalitzat: %s" % (actual)+'\r\n')
          actual.close()
          conns.remove(actual)
          continue
        while data:
          if EOT not in data:
            data=data+s.recv(1024)
          else:
            data=data[:-1]
            break
      except Exception:
        continue  
      comm = data.split(' ')
      if len(comm) !=2:
        actual.sendall('Son necesarios 2 argumentos'+EOT)
        continue
      ordre = comm[0]
      path =comm[1][:-1]
      if ordre not in ['ls','get']:
        actual.sendall('Ordenes no validas'+EOT)
        continue
      elif ordre == 'ls':
        command = ["/usr/bin/%s %s"%(ordre,path)]
        pipeData = Popen(command, shell=True,stdout=PIPE, stderr=PIPE)
        for line in pipeData.stdout:
          actual.sendall(line)
        for line in pipeData.stderr:
          actual.sendall(line)
        actual.sendall(EOT)
      else:
        try:
          fluxe_fitxer=open(path,'r')
          for line in fluxe_fitxer:
            actual.sendall(line)
          actual.sendall(EOT)
          fluxe_fitxer.close()
        except:
          actual.sendall('Error de lectura')
          actual.sendall(EOT)
s.close()
sys.exit(0)

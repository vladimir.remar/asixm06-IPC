#!/bin/bash
for e in $(systemctl list-unit-files | tr -s [:blank:]|cut -d' ' -f1|grep -E ".service$")
do 
  stat=$(systemctl is-active $e) 
  if [ "$stat" = "active" ]
  then 
      echo -E "$e $stat"
  fi
done

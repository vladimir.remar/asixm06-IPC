#!/usr/bin/python 
#-*- coding: utf-8-*-
# server trivial FTP
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# @edt Curs 2017-2018
# Vladimir remar
"""
Requeriments per al client (mode interactiu):
● Se li passen com a arguments opcionals el host (​ H a.b.c.d) i el port (​ p n) amb qui s'ha de
connectar. Si es donen els dos arguments fa la connexió immediatament.
● Admet ordres de l'usuari per l'stdin:
○ connect host:port. Permès només si encara no ha fet la connexió.
○ ls [path]. Li envia l'ordre al servidor i espera un llistat de fitxers que mostra per
stdout.
○ get [path]nom_fitxer. Li envia l'ordre al servidor i espera una seqüència de bytes que
grava en el directori actiu amb el nom indicat.
○ quit. Tanca la connexió amb el servidor i acaba.
● Cada vegada que el programa espera alguna cosa de l'usuari li escriu un prompt
(client_ftp_trivial>).
"""
#-----------------------------------------------------------------------

import sys,socket
import os,argparse

parser = argparse.ArgumentParser(description='Client FTP trivial')
parser.add_argument('-p','--port', dest='port', help="número de port amb qui s'ha de connectar", type=int,metavar="port-a-conectar")
parser.add_argument('-s','--host',dest='host',help="host amb qui s'ha de connectar",type=str,metavar="host-a-conectart")
args=parser.parse_args()

s=None
cap = 'client_ftp_trivial> '
EOT  = chr(4)

if args.port and args.host:
  HOST = args.host
  PORT = args.port
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s.connect((HOST, PORT))

while True:
  sys.stdout.write(cap)
  data =sys.stdin.readline()
  if data[:-1] == 'quit': break
  if not data: continue
  comm = data.split(' ')
  if len(comm) !=2:
    sys.stderr.write('CLIENT:Son necesarios 2 argumentos\r\n')
    continue
  ordre = comm[0]
  path =comm[1]
  if ordre not in ['ls','get','connect']:
    sys.stderr.write('CLIENT: Ordenes no validas\r\n')
    continue
  if not s:
    if ordre == 'connect':
      hostport=path.split(':')
      HOST=hostport[0]
      PORT=int(hostport[1])
      s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      s.connect((HOST, PORT))
      continue
    else:
      sys.stderr.write('CLIENT: Te tienes que conectar antes\r\n')
      sys.stderr.write('Try: host:port P.E. "connect localhost:50001" \r\n')
  else:
    if ordre == 'connect':
      sys.stderr.write('CLIENT: Ya estas conectado\r\n')
      continue
    else:
      s.sendall(ordre+' '+path+EOT)
      while True:
        dades = s.recv(1024)
        if ordre == 'ls':
          if EOT in dades:
            sys.stdout.write(dades[:-1])
            break
          sys.stdout.write(dades)
        elif ordre == 'get':
          try:
            path_name=path.split('/')
            file_name=path_name[-1]
            fitxer='%s'%(file_name[:-1])
            fluxe_fitxer=open(fitxer,'a')
            if EOT in dades:
              fluxe_fitxer.write(dades[:-1])
              fluxe_fitxer.close()
              break
            else:
              fluxe_fitxer.write(dades)
            fluxe_fitxer.close()
          except:
            sys.stderr.write('CLIENT: error de escritura\r\n')
            continue
if s is not None:
  s.close()
sys.exit(0)

#!/usr/bin/python 
#-*- coding: utf-8-*-
# exemple de echo client basic
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# @edt Curs 2017-2018
# Vladimir remar
# -----------------------------------------------------------------
import sys,socket
import argparse

parser = argparse.ArgumentParser(description='Client: sonsulta calendar')
parser.add_argument('-s',dest='HOST', help='host', metavar='host', required=True)
parser.add_argument('-p',dest='PORT', help='port per on escolta',type=int, metavar='port', required=True)
parser.add_argument('-y',dest='year', help='opcio year ', metavar='year', default='2018')
args = parser.parse_args()
HOST = args.HOST
PORT = args.PORT
YEAR = args.year
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
s.send(YEAR)
while True:
  data = s.recv(1024)
  print data
  if not data:
    break
s.close()
sys.exit(0)

# /usr/bin/python
#-*- coding: utf-8-*-
# vladimir
'''
Valida la següent synopsis que rep obligatoriament les opcions dels fitxers d’usuaris i
grups per processar-los segons el criteri per defecte login.
Sinopsys: programa [-s login | uid | gid | gname ] -u usuaris -g grups.
'''
import argparse
import sys

parser = argparse.ArgumentParser(description='mostrar linies d\'un fitxer')

parser.add_argument('-u', dest='usuaris', help='fitxer  d\'usuaris a processar', type=str,metavar="file-users", required=True)

parser.add_argument('-g', dest='grups', help='fitxer de grups a processar', type=str,metavar='file-group',required=True)

parser.add_argument('-s', dest='criteri', help='criteri per ordenar, default=login', default="login",choices=["login","uid","gid","gname"])

args=parser.parse_args()

print args
print args.usuaris
print args.grups
print args.criteri

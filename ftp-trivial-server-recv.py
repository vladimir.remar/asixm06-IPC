#!/usr/bin/python 
#-*- coding: utf-8-*-
# server trivial FTP
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# @edt Curs 2017-2018
# Vladimir remar
"""
Requeriments per al servidor:
● Se li passa com a argument (​ p n) el número de port que ha d'escoltar.
● Admet un sol client a l'hora.
● Accepta les següents ordres del client:
○ ls [path]. Retorna al client el llistat de fitxers del directori actiu.
○ get [path]nom_fitxer. Retorna al client el contingut de nom_fitxer.
● Quan el client acaba, el servidor es queda esperant una altra connexió.
● Acaba ordenadament quan rep un SIGTERM.
"""
# -----------------------------------------------------------------
import sys,socket
import os,argparse,signal
from subprocess import Popen, PIPE

def mysigterm(signum,frame):
  print 'Signal handler called with signal', signum
  print 'Adeu'
  sys.exit(0)

#signal.signal(signal.SIGTERM, mysigterm)
#signal.signal(signal.SIGINT, signal.SIG_IGN)

parser = argparse.ArgumentParser(description='Servidor FTP trivial')
parser.add_argument('-p','--port', dest='port', help="número de port que ha d'escoltar", type=int,metavar="port-a-escoltar", required=True)
args=parser.parse_args()

HOST = ''
PORT = args.port
EOT  = chr(4)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)

print os.getpid()

while True:
  try:
    conn, addr = s.accept()
  except Exception:
    continue
  while True:
    data = conn.recv(1024)
    if not data: break
    
    while data:
      if EOT not in data:
        data=data+s.recv(1024)
      else:
        data=data[:-1]
        break
        
    comm = data.split(' ')
    if len(comm) !=2:
      conn.sendall('Son necesarios 2 argumentos'+EOT)
      continue
    ordre = comm[0]
    path =comm[1][:-1]
    if ordre not in ['ls','get']:
      conn.sendall('Ordenes no validas'+EOT)
      continue
    elif ordre == 'ls':
      command = ["/usr/bin/%s %s"%(ordre,path)]
      pipeData = Popen(command, shell=True,stdout=PIPE, stderr=PIPE)
      for line in pipeData.stdout:
        conn.sendall(line)
      for line in pipeData.stderr:
        conn.sendall(line)
      conn.sendall(EOT)
    else:
      try:
        fluxe_fitxer=open(path,'r')
        for line in fluxe_fitxer:
          conn.sendall(line)
        conn.sendall(EOT)
        fluxe_fitxer.close()
      except:
        conn.sendall('Error de lectura')
        conn.sendall(EOT)
  conn.close
s.close()
sys.exit(0)

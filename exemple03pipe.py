#!/usr/bin/python
#-*- coding: utf-8-*-
'''
# llista un directori
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# @edt Curs 2014-2015 Desembre 2014
# -----------------------------------------------------------------
'''
import sys
from subprocess import Popen, PIPE
import argparse

parser = argparse.ArgumentParser(description='%prog [options] [arg]\ntype %prog -h or%prog --help per a mes informacio')
parser.add_argument(dest='ruta', help='Ruta a llistar',metavar='ruta-a-llistars')
args = parser.parse_args()

# Executa el pipe/subprocess
command = ["ls", args.ruta]
pipeData = Popen(command, stdout=PIPE, stderr=PIPE)

# Sortida
for line in pipeData.stdout:
	print line

#print pipeData.stdout.read(),

sys.exit(0)

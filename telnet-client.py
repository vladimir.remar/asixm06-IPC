#!/usr/bin/python 
#-*- coding: utf-8-*-
# telnet Client
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# @edt Curs 2017-2018
# Vladimir remar
# -----------------------------------------------------------------
import sys,socket,os
HOST = ''
PORT = 50001
EOT  = chr(4)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

while True:
  ordre = raw_input()
  if ordre == 'quit': break
  if not ordre: continue
  s.sendall(ordre)
  while True:
    dades = s.recv(1024)
    if dades[-1] == EOT:
      print dades[:-1]
      break
    print dades
s.close()
sys.exit(0)

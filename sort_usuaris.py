# /usr/bin/python
#-*- coding: utf-8-*-
# vladimir
'''
Valida la següent synopsis on per defecte es processa stdin amb el criteri
d’ordenació per login.
Synopsis: sort_usuaris [-s login|uid|name] [-f fitxer]
'''

import argparse
import sys

parser = argparse.ArgumentParser(description='mostrar linies d\'un fitxer ordenadas')

parser.add_argument('-f', dest='fitxer', help='fitxer a processar, default=/dev/stdin', type=str, default='/dev/stdin')

parser.add_argument('-s', dest='criteri', help='criteri per ordenar, default=login', default="login",choices=["login","uid","name"])

args=parser.parse_args()

print args
print args.fitxer
print args.criteri

sys.exit()

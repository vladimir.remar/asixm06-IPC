#!/usr/bin/python 
#-*- coding: utf-8-*-
# CLient ps -ax
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# @edt Curs 2017-2018
# Vladimir remar
# -----------------------------------------------------------------
import sys,socket
import os
from subprocess import Popen, PIPE

HOST = ''
PORT = 50001
EOT  = chr(4)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

command = ["/usr/bin/ps -ax"]
pipeData = Popen(command, shell=True,stdout=PIPE, stderr=PIPE)

for line in pipeData.stdout:
  s.sendall(line)
s.close()
sys.exit(0)


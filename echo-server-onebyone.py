#!/usr/bin/python 
#-*- coding: utf-8-*-
# Server echo one by one
# -----------------------------------------------------------------
# Vladimir
# Escola del treball de Barcelona
# ASIX Hisix2 M06-ASO UF2NF1-Scripts
# -----------------------------------------------------------------
import sys,socket
HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)

while True:
  conn, addr = s.accept()
  print 'Connected by', addr
  while True:
    data = conn.recv(1024)
    if not data: break
    conn.send(data)
  conn.close()
sys.exit(0)

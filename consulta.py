#!/usr/bin/python
#-*- coding: utf-8-*-
'''
# Consultar dades que tenim a Postgres mitjançant una pipe amb PSQL.
# Select * from clientes.
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# ##########################################
'''
import sys
from subprocess import Popen, PIPE

NOM_BASE = 'lab_clinic'
consulta = "select * from pacients;\q\n"
command = "psql -qtA -F ';' %s" %(NOM_BASE)
command_remote = "psql -h 172.17.0.2 -U edtasixm06 training -c 'select * from oficinas;'"
# POPEN
pipeData = Popen(command, shell = True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
pipeData.stdin.write(consulta)

# Sortida
for line in pipeData.stdout.readlines():
	print line,
sys.exit(0)

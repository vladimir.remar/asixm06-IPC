#!/usr/bin/python 
#-*- coding: utf-8-*-
# Daytime client
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisix2 M06-ASO UF2NF1-Scripts
# -----------------------------------------------------------------
import sys,socket
HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
while True:
  data = s.recv(1024)
  if not data: break
  print data
s.close()
sys.exit(0)

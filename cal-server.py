#!/usr/bin/python 
#-*- coding: utf-8-*-
# exemple cal server basic
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# @edt Curs 2017-2018
# Vladimir remar
# -----------------------------------------------------------------
import sys,socket
import os,signal
from subprocess import Popen, PIPE

def mysigusr1(signum,frame):
  print "Signal handler called with signal:", signum
  global clients
  print "Lista clients",clients

def mysigusr2(signum,frame):
  print "Signal handler called with signal:", signum
  global clients
  print "Numero de clients:",len(clients)

def mysighup(signum,frame):
  clients=[]

def mysigalarm(signum,frame):
  print "Signal handler called with signal:", signum
  print "Adeu"
  sys.exit(0)

def mysigterm(signum,frame):
  print "Signal handler called with signal:", signum
  print "NONI"

signal.signal(signal.SIGALRM,mysigalarm)
signal.signal(signal.SIGTERM,mysigterm)
signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
signal.signal(signal.SIGHUP,mysighup)
#signal.signal(signal.SIGINT, signal.SIG_IGN)

HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)

clients= []
print "Inici del programa principal PARE"
pid = os.fork()
if pid != 0:
  print "Fi Programa PARE", os.getpid(), pid
  sys.exit(0)
print os.getpid()
while True:
  try:
    conn, addr = s.accept()
  except Exception:
    continue
  clients.append(addr)
  year=conn.recv(1024)
  # Executa el pipe/subprocess
  command = ["/usr/bin/cal -y %s"%(year)]
  pipeData = Popen(command, shell=True,stdout=PIPE, stderr=PIPE)

  for line in pipeData.stdout:
    conn.send(line)
  conn.close()

sys.exit(0)

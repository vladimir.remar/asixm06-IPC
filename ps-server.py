#!/usr/bin/python 
#-*- coding: utf-8-*-
# server ps -ax
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# @edt Curs 2017-2018
# Vladimir remar
# -----------------------------------------------------------------
import sys,socket
import os,time
from subprocess import Popen, PIPE

HOST = ''
PORT = 50001
EOT  = chr(4)
_DATE_FORMAT = "%Y-%m-%d-%H:%M:%S"

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)

print os.getpid()

while True:
  try:
    conn, addr = s.accept()
    t = time.strftime(_DATE_FORMAT, time.localtime(time.time()))
    name_fit ='%s'%(addr[0])+'.log'
    ff = open(name_fit, 'ab')# 'ab' to create if no exists or open to append
    ff.write('Host: '+addr[0]+' logtime: '+t+'\n')
  except Exception:
    continue
  try:
    while True:
      data = conn.recv(1024)
      if not data: break
      ff.write(data)
    ff.close()
  except:
    continue
  conn.close()
sys.exit(0)

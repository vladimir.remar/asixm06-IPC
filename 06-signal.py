# /usr/bin/python
#-*- coding: utf-8-*-
# valdimir
# signals modificats

import argparse
import sys
import signal, os

PID = os.getpid()

parser = argparse.ArgumentParser(description='Alarm control')
parser.add_argument(dest='minuts',type=int,help=' minuts de alarma',metavar='numero-minuts')
args = parser.parse_args()


ups = 0
downs = 0

def alarma(signum, frame):
  print 'Signal handler called with signal', signum
  print 'Time'
  segons = signal.alarm(0)
  print segons
  segons = signal.alarm(segons)
 

def uppers(signum, frame):
  global ups
  print 'Signal handler called with signal', signum
  print 'UP'
  ups = ups +1
  segons = signal.alarm(0)
  segons = signal.alarm(segons+60)

def downs(signum, frame):
  global downs
  print 'Signal handler called with signal', signum
  print 'DOWN'
  downs = downs -1
  segons = signal.alarm(0)
  segons = signal.alarm(segons-60)
  
def recarrega(signum, frame):
  #global args.minuts
  print 'Signal handler called with signal', signum
  print 'Reload alarm'
  signal.alarm(args.minuts)

def mysigterm(signum,frame):
  print 'Signal handler called with signal', signum
  print signal.alarm(0),uppers,downs
  sys.exit(0)

signal.signal(signal.SIGALRM, alarma)
signal.signal(signal.SIGHUP, recarrega)
signal.signal(signal.SIGTERM, mysigterm)
signal.signal(signal.SIGUSR1, uppers)
signal.signal(signal.SIGUSR2, downs)
signal.signal(signal.SIGINT, signal.SIG_IGN)
signal.alarm(args.minuts*60)

print PID
while True:
  pass
sys.exit(0)

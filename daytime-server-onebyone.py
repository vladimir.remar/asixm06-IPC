#!/usr/bin/python 
#-*- coding: utf-8-*-
# Daytime server
# -----------------------------------------------------------------
# Vladimir
# Escola del treball de Barcelona
# ASIX Hisxi2 M06-ASO UF2NF1-Scripts
# -----------------------------------------------------------------
import sys,socket
import os,signal
from subprocess import Popen, PIPE

HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)

while True:
  conn, addr = s.accept()
  print 'Connected by', addr

  command = ["/usr/bin/cal"]
  pipeData = Popen(command, stdout=PIPE, stderr=PIPE)

  for line in pipeData.stdout:
    conn.send(line)
  conn.close()
# -----------------------------------------------



sys.exit(0)
